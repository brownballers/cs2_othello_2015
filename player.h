#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    
    //Board variable
    Board *board;
    
    Side AI_side;
    Side opp_side;
    int get_move_score(Move *move_considered, Board *board_used);
};

#endif
