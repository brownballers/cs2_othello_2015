We worked on the project together and as such any code was created as a dual effort. In the first week we focussed 
on implementing a 2-depth minmax tree and figuring out the huristics for the game. 

This week we extended our work by adding another depth to our minmax tree and added certain optimizations to avoid 
searching through all moves when one was obviously better. For example, we made our AI prioritize corner positions and 
take those whenever possible. This strategy allowed us to consistently beat the BetterPlayer AI. 

Each of us contributed equally and all strategies were discussed as a team before implementation. 

The improvements made to make the AI tournament worthy include
- Increasing the depth of the minmax tree
- Prioritizing certain postions on the board 
- Penalizing moves that allowed the opponent access to key positions
- We attempted some speed optimizations through alpha/beta pruning 


 