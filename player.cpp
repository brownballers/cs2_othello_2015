#include "player.h"
#include <vector>
//Commit Saagar
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
// Modification by Sreenivas Appasani
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = true;

	//Initialize sides   
	board = new Board();
     AI_side = side;
     if (side == BLACK)
     { opp_side = WHITE;}
     else 
     { opp_side = BLACK;}   
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

int Player::get_move_score(Move *move_considered, Board *board_used) {
	 // initialize the number of opponent moves after this move
		 int num_opp_moves = 0;  
		 int move_score = (board_used->count(AI_side) - board_used->count(opp_side));
		 int move_x = move_considered->getX();
		 int move_y = move_considered->getY();
		 // increase score if move is an edge
		 if ((move_x == 0) or (move_x == 7) or (move_y == 0) or (move_y == 7)) {
			 move_score += 2;
		 }
		 if ((move_x == 0 and move_y == 1) or 
		 (move_x == 1 and move_y == 0) or 
		 (move_x == 6 and move_y == 0) or 
		 (move_x == 7 and move_y == 1) or 
		 (move_x == 0 and move_y == 6) or 
		 (move_x == 1 and move_y == 7) or 
		 (move_x == 7 and move_y == 6) or 
		 (move_x == 6 and move_y == 7)) {
			 move_score -= 10;   
		 }
		 // increase score even more if move is a corner
		 if ((move_x == 0 and move_y == 0) 
		 or (move_x == 7 and move_y == 7) 
		 or (move_x == 0 and move_y == 7) 
		 or (move_x == 7 and move_y == 0)) {
			 move_score += 10;
		 }
		 // Penalize moves that are directly adjacent to corners since opponent can then take these squares
		 else if ((move_x == 1 and move_y == 1) 
		 or (move_x == 1 and move_y == 6)
		 or (move_x == 6 and move_y == 1) 
		 or (move_x == 6 and move_y == 6)) {
			 move_score -= 10;
		 }
		 // count the number of possible opponent moves
		 for (int l = 0; l < 8; l++) {
			  for (int j = 0; j < 8; j++) {
				  Move *move_opp = new Move(l, j);   
				  if (board->checkMove(move_opp, opp_side)) {
					  num_opp_moves++;
				  }
			  }
		 }
		 move_score -= (num_opp_moves * 2);   
		 return move_score;
	 }

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
     
     if (opponentsMove != NULL) {
		board->doMove(opponentsMove, opp_side);
	 }
	 vector<Move *> possible_moves;     
	 int move_score; 
     if (board->hasMoves(AI_side)) {
		 for (int i = 0; i < 8; i++) {
			 for (int j = 0; j < 8; j++) {
				 Move *move = new Move(i, j);   
				 if (board->checkMove(move, AI_side)) {
					 possible_moves.push_back(move);
				 }
			 }
		 }
	 }
	 else {
         return NULL;
	 } 
	 vector<Move *>::iterator i;
	 vector<Move *>::iterator m;
	 vector<Move *>::iterator n;
	 vector<Move *>::iterator i2;
	 vector<int> min_scores;
	 Move * chosen_move;   
	 int is_corner = 0;   
	 for (i = possible_moves.begin(); i != possible_moves.end(); i++) {
		 int move_x = (*i)->getX();   
	     int move_y = (*i)->getY();
		 if ((move_x == 0 and move_y == 0) 
			or (move_x == 7 and move_y == 7) 
			or (move_x == 0 and move_y == 7) 
			or (move_x == 7 and move_y == 0)) { 
				chosen_move = (*i); 
				is_corner = 1;
				break;
			}
		 Board *board_copy = board->copy();
		 board_copy->doMove(*i, AI_side);   
		 vector<Move *> possible_moves_opp;
		 // for each possible moves, go down one layer and get the possible   
		 // opponent's moves
		 int min_score = 100; 
		 int min_score_2 = 100;
		 if (board_copy->hasMoves(opp_side)) {
			 for (int i = 0; i < 8; i++) {
				 for (int j = 0; j < 8; j++) {
					 Move *move = new Move(i, j);   
					 if (board_copy->checkMove(move, opp_side)) {
						 possible_moves_opp.push_back(move);
					 }
				 }
			 }	
			    
			 // Calculate the minimum score of all of the possible opponent's moves
			 for (m = possible_moves_opp.begin(); m != possible_moves_opp.end(); m++) {
				 move_x = (*m)->getX();
				 move_y = (*m)->getY();
				 if ((move_x == 0 and move_y == 0) 
				 or (move_x == 7 and move_y == 7) 
				 or (move_x == 0 and move_y == 7) 
				 or (move_x == 7 and move_y == 0)) {  
					 break;
				 }
				 Board *board_copy_opp = board_copy->copy();
				 board_copy_opp->doMove(*m, opp_side);
				 vector<Move *> possible_moves_AI2;
				 if (board_copy_opp->hasMoves(AI_side)){
					for (int i = 0; i < 8; i++) {
						for (int j = 0; j < 8; j++) {
							Move *move = new Move(i, j);   
							if (board_copy_opp->checkMove(move, AI_side)) {
								possible_moves_AI2.push_back(move);
							}
						}
					}  	 
				
					for (n = possible_moves_AI2.begin(); n != possible_moves_AI2.end(); n++) {
						Board *board_copy_AI2 = board_copy_opp->copy();
						board_copy_AI2->doMove(*n, AI_side);
						move_score = get_move_score(*n, board_copy_AI2); 
						if (move_score < min_score) {
							min_score = move_score;
						}
					}
					if (min_score < min_score_2) {
						min_score_2 = min_score;   
					}
				}
				else {
					min_score = get_move_score(*m, board_copy_opp);
					if (min_score < min_score_2) {
						min_score_2 = min_score;
					}
				}
			}
			if ((move_x == 0 and move_y == 0) 
				 or (move_x == 7 and move_y == 7) 
				 or (move_x == 0 and move_y == 7) 
				 or (move_x == 7 and move_y == 0)) {
					 min_scores.push_back(-1000); 
			}
			else {
			min_scores.push_back(min_score_2);  
			}
		} 
		 // If there are no opponent's moves, then just use the score of the possible move considered   
		 else {
			 move_score = get_move_score(*i, board_copy);
			 min_scores.push_back(move_score);
		 }
	 }
	 if (is_corner == 1) {
		 board->doMove(chosen_move, AI_side);   
	     return chosen_move;
	 }
	 unsigned int max_index = 0;   
	 int max_score = min_scores[0];   
	 // Use the possible move considered that has the highest of the minimum of the opponent's move's scores   
	 for (unsigned int q = 0; q < min_scores.size(); q++) {  
		 if (min_scores[q] > max_score) {
			 max_index = q;
		 }
	 }
	 chosen_move = possible_moves[max_index];
     board->doMove(chosen_move, AI_side);   
	 return chosen_move;
}
